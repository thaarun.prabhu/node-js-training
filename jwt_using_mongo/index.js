const express = require('express');
const mongoose = require('mongoose');
const authRoute = require('./routes/auth');
const app = express();

app.use(express.json());

// connect to db
mongoose.connect(
	'mongodb://localhost:27017/AdminBro',
	{ useNeWUrlParser: true },
	() => console.log('connected to database')
);

// routes middlewares
app.use('/api/user', authRoute);

const port = 3000;

app.listen(port, () => {
	console.log('server running in 3000');
});
