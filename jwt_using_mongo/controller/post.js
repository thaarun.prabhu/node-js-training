const User = require('../model/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
// const verify = require('./validateToken');

exports.test = async (req, res) => {
	const getall = await user.find();
	res.json(getall);
};

// post
exports.userCreate = async (req, res) => {
	// bcrypt
	const salt = await bcrypt.genSalt(10);
	const hashPassword = await bcrypt.hash(req.body.password, salt);

	const user = new User({
		name: req.body.name,
		email: req.body.email,
		password: hashPassword,
	});

	// jwt
	const token = jwt.sign({ _id: user.id }, 'hellojwt');
	res.header('auth-token', token).send(token);
	try {
		const savedUser = await user.save();
		res.send(savedUser);
	} catch (err) {
		res.status(400).send(err);
	}
};

exports.user_details = function (req, res) {
	User.findById(req.params.id, function (err, user) {
		if (err) return next(err);
		res.send(user);
	});
};

exports.user_update = function (req, res) {
	User.findByIdAndUpdate(
		req.params.id,
		{ $set: req.body },
		function (err, user) {
			if (err) {
				return next(err);
			} else {
				res.send('Product udpated.', user);
			}
		}
	);
};

exports.user_delete = function (req, res) {
	User.findByIdAndRemove(req.params.id, function (err) {
		if (err) return next(err);
		res.send('Deleted successfully!');
	});
};
