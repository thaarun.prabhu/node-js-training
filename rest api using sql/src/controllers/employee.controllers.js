const employeeModel = require('../models/employee.model');

exports.getEmployeeList = (req, res) => {
	// console.log('employee list');
	employeeModel.getAllEmployee((err, employee) => {
		if (err) {
			res.send(err);
		} else {
			res.send(employee);
		}
	});
};
