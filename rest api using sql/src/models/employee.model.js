const dbConn = '../../config/db.config.js';

var Employee = (employee) => {
	this.name = employee.name;
	this.company = employee.company;
	this.salary = employee.salary;
	this.phone = employee.phone;
};

// get all employee

Employee.getAllEmployee = (result) => {
	dbConn.query('select * from employee', (err, res) => {
		if (err) {
			console.log('error while fetching employees', err);
			result(null, err);
		} else {
			console.log('Employee fetched sucessfully');
			result(null, res);
		}
	});
};

module.exports = Employee;
