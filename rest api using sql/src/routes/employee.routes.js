const express = require('express');
const routers = express.Router();

const employeControler = require('../controllers/employee.controllers');

routers.get('/', employeControler.getEmployeeList);

module.exports = routers;
