const express = require('express');
const employeeRoutes = require('./src/routes/employee.routes');

const app = express();

app.use('/api/employee', employeeRoutes);

const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
	res.send('hello res api');
});

app.listen(port, () => {
	console.log(`server running at post ${port}`);
});
