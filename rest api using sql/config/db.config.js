const mysql = require('mysql');

// connection

const dbConn = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'crud',
});

dbConn.connect((err) => {
	if (err) {
		console.log(err);
	} else {
		console.log('connected sucessfully');
	}
});

module.exports = dbConn;
