const { Router } = require('express');
const express = require('express');
const router = express.Router();
const Person = require('./PersonScheme');

router.get('/', (req, res) => {
	res.json('router is Working');
});

router.post('/', async (req, res) => {
	const TopBanner = new Person(req.body);
	TopBanner.save(function (err, response) {
		if (err) {
			console.error('ERROR: in save topbanner: ', err.message);
			res.send({ status: 'error', message: err.message });
		} else if (!response) {
			res.send({ status: false, message: 'not saved' });
		} else {
			res.send({
				status: true,
				message: 'saved topBanner',
				TopBannerDetails: response,
			});
		}
	});
	// const postPerson = 	{
	// 	Name: req.body.Name,
	// 	Age: req.body.Age,
	// };
	// const postPerson = await Person.save();
	// res.status(200).json(postPerson);
});

module.exports = router;
