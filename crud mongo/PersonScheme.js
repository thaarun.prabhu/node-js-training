const mongoose = require('mongoose');

const PersonScheme = mongoose.Schema({
	Name: {
		type: String,
		require: true,
	},
	Age: {
		type: String,
		require: true,
	},
	CreateTime: {
		type: Date,
		default: Date.now,
	},
});

module.export = mongoose.model('Persons', PersonScheme);
