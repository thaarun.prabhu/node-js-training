const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const uuid = require('uuid');
require('dotenv/config');
const app = express();
app.use(express.json());
// router
const personRouter = require('./Person');
app.use('/person', personRouter);

const PORT = 3000;

// body parser
app.use(morgan('dev'));

app.get('/', function (req, res) {
	res.json(myPerson);
});

app.get('/:id', async (req, res) => {
	const getOne = await myPerson.filter((e) => e.id === req.params.id);
	res.status(200).json(getOne);
});

app.post('/', async (req, res) => {
	myPerson.push(req.body);
	console.log(req.body);
});

app.get('/about', function (req, res) {
	res.json('about');
});

app.listen(PORT, () => {
	console.log('server started on 3000');
});

const DB_URI = 'mongodb://localhost:27017/AdminBro';

// Database connection
// mongoose.connect(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true },);
// const connection = mongoose.connection;
// connection(process.env.MY_DB, (err) => {
// if (err) {
// 	console.log('db not connect');
// } else {
// 	console.log('db connect successfully');
// }
// });

// https://www.codegrepper.com/code-examples/whatever/local+mongodb+url
mongoose.connect('mongodb://localhost:27017/AdminBro', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

const connection = mongoose.connection;

if (connection) {
	console.log('db connected');
}
