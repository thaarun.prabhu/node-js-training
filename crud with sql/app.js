const express = require('express');
const mysql = require('mysql');

const app = express();

express.json();

const db = mysql.createConnection({
	host: 'localhost:3306',
	user: 'root',
	password: '',
	database: 'test',
});

// connect
db.connect((err) => {
	if (err) {
		console.log(err);
	} else {
		console.log('Mysql connection...');
		console.log('Mysql connection...');
	}
});

app.get('/createdb', (req, res) => {
	let sql = 'CREATED DATABASE mysql';
	db.query(sql, (err, result) => {
		if (err) {
			console.log(err);
		}
		console.log(result);
		res.send('database created');
	});
});
app.listen('3000', () => {
	console.log('server started at port 3000');
});
