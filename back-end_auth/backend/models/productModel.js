import mongoose from 'mongoose';

const productSchema = new mongoose.Schema(
	{
		startFrom: { type: String, required: true },
		destination: { type: String, required: true },
		description: { type: String, required: true },
		location: { type: String, required: true },
		// thumbnailImage: { type: String, required: true },
		// image: { type: String, required: true },
	},
	{
		timestamps: true,
	}
);

const Product = mongoose.model('Product', productSchema);

export default Product;
