import bcrypt from 'bcryptjs';

const data = {
	users: [
		{
			name: 'thaarun',
			email: 'admn@example.com',
			password: bcrypt.hashSync('1234', 8),
			isAdmin: true,
		},
		{
			name: 'nivas',
			email: 'admi@example.com',
			password: bcrypt.hashSync('1234', 8),
			isAdmin: true,
		},
	],
	products: [
		{
			startFrom: 'nike',
			destination: 'shoes',
			description: 120,
			location: 'nike',
		},
		{
			startFrom: 'rebook',
			destination: 't-shirt',
			description: 120,
			location: 'rebook',
		},
	],
};

export default data;
