import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import userRouter from './routers/userRouter.js';
import productRouter from './routers/productRouter.js';

dotenv.config();

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect('mongodb://localhost/myBack', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

app.use('/api/users', userRouter);
app.use('/api/products', productRouter);

app.use((err, req, res, next) => {
	res.status(500).send({ message: err.message });
});

app.get('/', (req, res) => {
	res.send('server is ready');
});

const port = process.env.PORT || 5000;

app.listen(port, () => {
	console.log(`server at local host ${port}`);
});
