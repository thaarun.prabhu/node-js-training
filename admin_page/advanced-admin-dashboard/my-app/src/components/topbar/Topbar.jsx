import React from 'react';
import './topbar.css';
import NotificationsNoneOutlinedIcon from '@material-ui/icons';

function Topbar() {
	return (
		<div className='topbar'>
			<div className='topbarWarpper'>
				<div className='topleft'>
					<span className='logo'>a</span>
				</div>
				<div className='topright'>
					<div className='topbaricons'>
						<NotificationsNoneOutlinedIcon />
						<span className='topIconBar'>2</span>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Topbar;
