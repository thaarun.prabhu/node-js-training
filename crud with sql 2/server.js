const mysql = require('mysql');
const express = require('express');

var app = express();

app.use(express.json());

var mysqlConnection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'sample',
	multipleStatements: true,
});

mysqlConnection.connect((err) => {
	if (!err) {
		console.log('working');
	} else {
		console.log('error' + err);
	}
});

app.listen(3000, () => {
	console.log('server running on port : 3000');
});

// get all test

app.get('/test', (req, res) => {
	mysqlConnection.query('select * from test', (err, rows, fields) => {
		if (!err) {
			res.send(rows);
		} else {
			console.log(err);
		}
	});
});

// get test by id

app.get('/test/:id', (req, res) => {
	mysqlConnection.query(
		'select * from test where test_id = ?',
		[req.params.id],
		(err, rows, fields) => {
			if (!err) {
				res.send(rows);
			} else {
				console.log(err);
			}
		}
	);
});

// delete

app.delete('/test/:id', (req, res) => {
	mysqlConnection.query(
		'delete  from test where test_id = ?',
		[req.params.id],
		(err, rows, fields) => {
			if (!err) {
				res.send('deleted successfully');
				console.log('deleted');
			} else {
				console.log(err);
			}
		}
	);
});

// post

app.get('/testpost', (req, res) => {
	let test = { name: 'hello' };
	var sql = 'insert into test set ?';
	mysqlConnection.query(sql, test, (err, rows, fields) => {
		if (!err) {
			res.send(rows);
			console.log('posted');
		} else {
			console.log(err);
		}
	});
});

app.get('/testpost2', (req, res) => {
	let test = { name: 'miskin' };
	var sql = 'insert into test set ?';
	mysqlConnection.query(sql, test, (err, rows, fields) => {
		if (!err) {
			res.send(rows);
		} else {
			console.log(err);
		}
	});
});

app.get('/testupdate/:id', (req, res) => {
	let newTitle = 'sharu';
	var sql = `update test set name = ${newTitle} where = ${req.params.id}`;
	mysqlConnection.query(sql, (err, rows, fields) => {
		if (!err) {
			res.send('updated');
		} else {
			console.log(err);
		}
	});
});
