const cookieParser = require('cookie-parser');
const express = require('express');
const app = express();

const { OAuth2Client } = require('google-auth-library');
const CLIENT_ID =
	'945408790745-b4sgdmc12fr9bualgvnkmd3n8m26ee7b.apps.googleusercontent.com';
const client = new OAuth2Client(CLIENT_ID);

const PORT = process.env.PORT || 5000;

// middleware
app.set('view engine', 'ejs');
app.use(express.json());
app.use(cookieParser());

app.get('/', (req, res) => {
	res.render('index');
});

app.get('/login', (req, res) => {
	res.render('login');
	let user = req.user;
	res.send(user.name);
});

app.post('/login', ckeckAuthenticated, (req, res) => {
	let user = req.user;

	let token = req.body.token;
	console.log(token);

	async function verify() {
		const ticket = await client.verifyIdToken({
			idToken: token,
			audience: CLIENT_ID,
		});

		const payload = ticket.getPayload();
		const userid = payload['sub'];
		console.log(payload);
	}
	verify()
		.then(() => {
			res.cookie('session', token);
			res.send('success');
			console.log(name);
		})
		.catch(console.error);
});

// app.get('/dashboard', (req, res) => {
// 	let user = req.user;
// 	res.render('dashboard', { user });
// });

app.get('/protectedroutes', (req, res) => {
	res.render('productedroutes');
});

function ckeckAuthenticated(req, res, next) {
	let token = req.cookies['session'];
	let user = {};
	async function verify() {
		const ticket = await client.verifyIdToken({
			idToken: token,
			audience: CLIENT_ID,
		});

		const payload = ticket.getPayload();

		user.name = payload.name;
		user.email = payload.email;
		user.picture = payload.picture;

		var name = payload.name;

		console.log(name);
	}
	verify()
		.then(() => {
			res.user = user;
			next();
		})
		.catch((err) => {
			res.redirect('/login');
		});
}

app.listen(PORT, () => {
	console.log(`server running on port ${PORT}`);
});
