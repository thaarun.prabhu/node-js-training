const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const postRoute = require('./routers/posts.js');

require('dotenv/config');

const app = express();
const port = 3000;

app.use(bodyParser.json());

app.use('/posts', postRoute);

app.get('/', (req, res) => {
	res.send('Hello World!');
});
try {
	mongoose.connect(process.env.URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});
} catch (err) {
	console.log(err);
}

app.listen(port);
