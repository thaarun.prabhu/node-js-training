const express = require('express');

const router = express.Router();

const Post = require('../models/Posts.js');

router.get('/', (req, res) => {
	res.send('Hello World! On post');
});

router.post('/', async (req, res) => {
	const post = new Post({
		title: req.body.title,
	});
	try {
		const savePost = await post.save();
		res.json(savePost);
	} catch (err) {
		res.json({ message: 'hello' });
	}
});

module.exports = router;
